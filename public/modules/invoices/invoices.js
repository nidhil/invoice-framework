angular.module('application')
.directive('invoicesModule',function($http,$rootScope){
    return {
        restrict:'E',
        templateUrl:'modules/invoices/invoices.html',
        link:function(scope,elem){
            scope.fn={
                newInvoice:function(){
                    scope.var.invoice.customer_name='0';
                    var par={
                        customer_id:parseInt(scope.var.invoice.customer_name),
                        discount:parseFloat(scope.fn.getTotalDiscount()),
                        total:parseFloat(scope.fn.getAllTotal())
                    }
                    $http.post('/api/invoices',par).success(function(v){
                        scope.var.invoice.id=v.id;
                        scope.var.invoice={
                            id:v.id,
                            customer_name:v.customer_id+'',
                            product:'-1',
                            products:[],
                            discount:v.discount
                        }
                        scope.fn.getInvoices();
                        $('#new-invoice').modal('show');
                    });
                },
                editInvoice:function(a){
                    $http.get('/api/invoices/'+a).success(function(v){
                        $http.get('/api/invoices/'+a+'/items').success(function(vi){
                            var prods=[];
                            for(var i=0;i<vi.length;i++){
                                var prod=scope.fn.getProductInfo(vi[i].product_id);
                                prods.push({
                                    id:vi[i].id,
                                    product_id:vi[i].product_id,
                                    name:prod.name,
                                    price:prod.price,
                                    quantity:vi[i].quantity
                                })
                            };
                            scope.var.invoice={
                                id:v.id,
                                customer_name:v.customer_id+'',
                                product:'-1',
                                products:prods,
                                discount:v.discount
                            }
                            
                            $('#new-invoice').modal('show');
                        });
                    });
                },
                deleteInvoice:function(a){
                    $http.delete('/api/invoices/'+a.id,).success(function(v){
                        scope.fn.getInvoices();
                    });
                },
                deleteItem:function(a){
                    $http.delete('/api/invoices/'+scope.var.invoice.id+'/items/'+scope.var.invoice.products[a].id).success(function(v){
                        scope.var.invoice.products.splice(a,1);
                        scope.fn.updateCustomer();
                    });
                },
                updateQuantity:function(a){
                    var par={
                        product_id:a.product_id,
                        quantity:a.quantity
                    };
                    $http.put('/api/invoices/'+scope.var.invoice.id+'/items/'+a.id,par).success(function(v){
                        scope.fn.updateCustomer();
                    });
                },
                updateCustomer:function(){
                    var par={
                        customer_id:parseInt(scope.var.invoice.customer_name),
                        discount:parseFloat(scope.fn.getTotalDiscount()),
                        total:parseFloat(scope.fn.getAllTotal())
                    };
                    $http.put('/api/invoices/'+scope.var.invoice.id,par).success(function(v){
                        scope.fn.getInvoices();
                    });
                },

                addProductToList:function(){
                    if(scope.var.invoice.product=='-1'){
                        return false;
                    }
                    var ind=parseInt(scope.var.invoice.product);
                    for(var i=0;i<scope.var.invoice.products.length;i++){
                        if(scope.var.invoice.products[i].product_id==scope.var.products[ind].id){
                            return false;
                        }
                    }
                    var par={
                        product_id:scope.var.products[ind].id,
                        quantity:1
                    }
                    $http.post('/api/invoices/'+scope.var.invoice.id+'/items',par).success(function(v){
                        var prod=scope.fn.getProductInfo(v.product_id);
                        var p={
                            id:v.id,
                            product_id:v.product_id,
                            name:prod.name,
                            price:prod.price,
                            quantity:'1'
                        }
                        scope.var.invoice.products.push(p);
                        scope.fn.updateCustomer();
                    });
                    
                },
                getProductInfo:function(id,a){
                    for(var i=0;i<scope.var.products.length;i++){
                        if(scope.var.products[i].id==id){
                            if(a==undefined){
                                return scope.var.products[i];
                            }else{
                                return scope.var.products[i][a];
                            }
                        }
                    }
                },
                getEachTotal:function(p){
                    return parseFloat((parseFloat(p.quantity)*parseFloat(p.price)).toFixed(2));
                },
                getTotalPrice:function(){
                    var tot=0.0;
                     for(var i=0;i<scope.var.invoice.products.length;i++){
                        tot=tot+scope.fn.getEachTotal(scope.var.invoice.products[i]);
                    }
                    return parseFloat((tot).toFixed(2));
                },
                getTotalDiscount:function(){
                    return parseFloat((parseFloat(scope.fn.getTotalPrice())*parseFloat(scope.var.invoice.discount)/100).toFixed(2));
                },
                getAllTotal:function(){
                    return parseFloat((scope.fn.getTotalPrice()-scope.fn.getTotalDiscount()).toFixed(2));
                },
                getInvoices:function(){
                    $http.get('/api/invoices').success(function(v){
                        for(var i=0;i<v.length;i++){
                            var cust=scope.fn.getCustomerInfo(v[i].customer_id);
                            if(!cust){
                                v[i].customer_name='Not set';
                            }else{
                                v[i].customer_name=cust.name;
                            }
                            
                        }
                        scope.var.invoices=v;
                    });
                },
                getCustomers:function(callback){
                    $http.get('/api/customers').success(function(v){
                        scope.var.customers=v;
                        callback();
                    });

                },
                getCustomerInfo:function(id,a){
                    for(var i=0;i<scope.var.customers.length;i++){
                        if(scope.var.customers[i].id==id){
                            if(a==undefined){
                                return scope.var.customers[i];
                            }else{
                                return scope.var.customers[i][a];
                            }
                        }
                    }
                    return false;
                },
                getProducts:function(){
                    $http.get('/api/products').success(function(v){
                        scope.var.products=v;
                    });
                },
                init:function(){
                    scope.var={
                        invoice:{
                            id:0,
                            customer_name:'0',
                            product:'-1',
                            products:[],
                            discount:'0'
                        },
                        products:[
                            {
                                id:1,
                                name:'Product A',
                                price:250
                            },
                            {
                                id:2,
                                name:'Product B',
                                price:250
                            }
                        ],
                        customers:[
                            {
                                id:1,
                                name:'Aditya',
                                address:'Kalyan',
                                phone:'9876543210'
                            },
                            {
                                id:2,
                                name:'Palash',
                                address:'Kalyan',
                                phone:'9876543211'
                            }
                        ]
                    };

                    
                    scope.fn.getCustomers(function(){
                        scope.fn.getInvoices();
                    });
                    scope.fn.getProducts();
                }
            }

            //init
            scope.fn.init();
        }
    }
})